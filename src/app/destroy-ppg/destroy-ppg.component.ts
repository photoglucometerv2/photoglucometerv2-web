import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PpgService } from '../services/ppg.service';

export interface DialogData {
    id: number
}

@Component({
    selector: 'app-destroy-ppg',
    templateUrl: './destroy-ppg.component.html',
    styleUrls: ['./destroy-ppg.component.css']
})
export class DestroyPpgComponent implements OnInit {
    alert = '&nbsp;'

    constructor(
        public dialogRef: MatDialogRef<DestroyPpgComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private ppgService: PpgService
    ) { }

    ngOnInit(): void {
    }

    onCancel() {
        this.dialogRef.close(false);
    }

    destroyPpg(){
        this.ppgService.destroy(this.data.id).subscribe(() => {
            this.dialogRef.close(true);
        }, (error) => {
            this.alert = error.message;
        });
    }
}

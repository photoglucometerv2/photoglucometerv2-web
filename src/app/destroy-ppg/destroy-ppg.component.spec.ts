import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DestroyPpgComponent } from './destroy-ppg.component';

describe('DestroyPpgComponent', () => {
  let component: DestroyPpgComponent;
  let fixture: ComponentFixture<DestroyPpgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DestroyPpgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DestroyPpgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

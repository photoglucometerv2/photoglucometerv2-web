import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModelComponent } from './model/model.component';
import { PpgComponent } from './ppg/ppg.component';
import { PpgsComponent } from './ppgs/ppgs.component';

const routes: Routes = [
    { path: '', redirectTo: '/ppgs', pathMatch: 'full' },
    { path: 'ppgs', component: PpgsComponent },
    { path: 'ppgs/model', component: ModelComponent },
    { path: 'ppgs/:id', component: PpgComponent},
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

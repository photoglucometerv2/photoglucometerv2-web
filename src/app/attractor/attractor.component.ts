import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

declare const Plotly: any;

@Component({
    selector: 'app-attractor',
    templateUrl: './attractor.component.html',
    styleUrls: ['./attractor.component.css']
})
export class AttractorComponent implements OnInit, AfterViewInit {
    @ViewChild('attractor', { static: false }) attractor?: ElementRef;
    @Input() series!: any;

    constructor() { }

    ngOnInit(): void {

    }

    ngAfterViewInit() {
        Plotly.newPlot(this.attractor?.nativeElement, [{
            type: 'scatter3d',
            mode: 'lines',
            x: this.series!.x,
            y: this.series!.y,
            z: this.series!.z,
            opacity: 1,
            line: {
                width: 6,
                reversescale: false
            }
        }], {
            width: document.documentElement.clientWidth,
            height: (document.documentElement.clientHeight - 64) * 0.7 - 48
        });
    }
}

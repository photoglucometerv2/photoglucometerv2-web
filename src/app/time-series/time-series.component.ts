import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

declare const Plotly: any;

@Component({
    selector: 'app-time-series',
    templateUrl: './time-series.component.html',
    styleUrls: ['./time-series.component.css']
})
export class TimeSeriesComponent implements OnInit, AfterViewInit {
    @ViewChild('timeSeries', { static: false }) timeSeries?: ElementRef;
    @Input() y!: number[];

    constructor() { }

    ngOnInit(): void {

    }

    ngAfterViewInit() {
        Plotly.newPlot(this.timeSeries?.nativeElement, [{
            type: 'scatter',
            x: Array.from(Array(this.y.length).keys()),
            y: this.y,
        }], {
            width: document.documentElement.clientWidth,
            height: Number(document.documentElement.clientHeight - 64) * 0.7 - 48
        });
    }
}

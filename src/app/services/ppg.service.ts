import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PpgIndexResponse, PpgShowResponse, PpgModelRequest, PpgModelResponse} from '../models/ppg';
import { HeadersService } from './headers.service';

@Injectable({
    providedIn: 'root'
})
export class PpgService {
    url = 'http://192.168.86.34:3000/ppgs'

    constructor(
        private http: HttpClient,
        private headersService: HeadersService
    ) { }

    index(page: number, pageSize: number): Observable<PpgIndexResponse> {
        return this.http.get<PpgIndexResponse>(`${this.url}`, this.headersService.buildHeaders()).pipe(
            catchError(this.handleError)
        );
    }

    show(id: number): Observable<PpgShowResponse> {
        return this.http.get<PpgShowResponse>(`${this.url}/${id}`, this.headersService.buildHeaders()).pipe(
            catchError(this.handleError)
        );
    }

    destroy(id: number): Observable<void> {
        return this.http.delete<void>(`${this.url}/${id}`, this.headersService.buildHeaders()).pipe(
            catchError(this.handleError)
        );
    }

    process(id: number): Observable<void> {
        return this.http.patch<void>(`${this.url}/${id}/process`, this.headersService.buildHeaders()).pipe(
            catchError(this.handleError)
        );
    }

    model(ppgModelRequest: PpgModelRequest): Observable<PpgModelResponse> {
        return this.http.post<PpgModelResponse>(`${this.url}/model`, ppgModelRequest, this.headersService.buildHeaders()).pipe(
            catchError(this.handleError)
        );
    }

    handleError(error: any) {
        return throwError(error.error);
    }
}

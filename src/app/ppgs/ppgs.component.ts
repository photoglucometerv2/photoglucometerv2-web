import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PpgIndexResponseItem } from '../models/ppg';
import { PpgService } from '../services/ppg.service';

@Component({
    selector: 'app-ppgs',
    templateUrl: './ppgs.component.html',
    styleUrls: ['./ppgs.component.css']
})
export class PpgsComponent implements OnInit {
    loading = true;
    ppgs: PpgIndexResponseItem[] = [];
    total = 0;
    page = 0;
    pageSize = 10;
    displayedColumns: string[] = [
        "select",
        "id",
        "bp_systolic",
        "bp_diastolic",
        "true_gluco",
        "subject_age",
        "subject_height",
        "subject_weight",
        "subject_sex",
        "subject_isdiabetic",
        "subject_email",
        "dim_corr",
        "hurst_exp",
        "lyapunov_exp",
        "details",
    ];
    dataSource = new MatTableDataSource<PpgIndexResponseItem>(this.ppgs);
    selection = new SelectionModel<PpgIndexResponseItem>(true, []);
    ppgActionForm = this.fb.group({
        type: ['', Validators.required]
    });
    selectedType = '';

    constructor(
        private ppgService: PpgService,
        private _snackBar: MatSnackBar,
        private fb: FormBuilder,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.ppgIndex();
    }

    ppgIndex() {
        this.ppgService.index(this.page, this.pageSize).subscribe((ppgIndexResponse) => {
            this.ppgs = ppgIndexResponse.ppgs;
            this.total = ppgIndexResponse.total;
            this.loading = false

            this.dataSource.data = this.ppgs;
            this.selection.clear();
        }, (error) => {
            this.openSnackBarAlert('Error al cargar contenido, intente más tarde', 'Entendido');
        });
    }

    changePage(event: any) {
        this.page = event.pageIndex;
        this.ppgIndex();
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.filteredData.filter((ppg) => !this.isProcessing(ppg)).length;
        return numSelected >= numRows;
    }

    isCheckboxIntermediate() {
        return this.selection.hasValue() && !this.isAllSelected();
    }

    toggleAllRows() {
        if (this.isAllSelected()) {
            this.selection.clear();
            return;
        }

        this.selection.select(...this.dataSource.filteredData.filter((ppg) => !this.isProcessing(ppg)));
    }

    openSnackBarAlert(message: string, action: string) {
        this._snackBar.open(message, action, {
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
            duration: 5000
        });
    }

    ppgAction() {
        if (this.ppgActionForm.value.type == '0') {
            this.processPpg();
        } else if (this.selection.selected.length >= 3) {
            this.router.navigate(['/ppgs/model'], {
                state: this.selection.selected.map((ppg) => ppg.id)
            });
        } else {
            this.openSnackBarAlert('Entrenar el modelo require por lo menos 3 filas de datos', 'Entendido');
        }
    }

    processPpg() {
        this.selection.selected.forEach((ppg) => {
            this.ppgService.process(ppg.id).subscribe(() => {
                this.ppgIndex();
                this.openSnackBarAlert('Solicitud enviada', 'Entendido');
            }, (error) => {
                this.openSnackBarAlert('Error al enviar la solicitud, inténtelo más tarde', 'Entendido');
            });
        })
    }

    isProcessing(ppg: PpgIndexResponseItem) {
        return ppg.processing_status_ppg == 0 || ppg.processing_status_alt == 0 || ppg.processing_status_ecg == 0;
    }
}

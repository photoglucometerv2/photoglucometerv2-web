import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PpgsComponent } from './ppgs.component';

describe('PpgsComponent', () => {
  let component: PpgsComponent;
  let fixture: ComponentFixture<PpgsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PpgsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PpgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { saveAs } from "file-saver";
import { PpgModelRequest, PpgModelResponse } from '../models/ppg';
import { PpgService } from '../services/ppg.service';

declare const Plotly: any;

@Component({
    selector: 'app-model',
    templateUrl: './model.component.html',
    styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {
    @ViewChild('matrix', { static: false }) matrix?: ElementRef;
    loading = true;
    model?: PpgModelResponse;

    constructor(
        private ppgService: PpgService,
        private router: Router,
        private route: ActivatedRoute,
        private _snackBar: MatSnackBar
    ) {
        const ppgModelRequest: PpgModelRequest = {
            ids: this.router.getCurrentNavigation()?.extras.state as number[]
        };

        if (ppgModelRequest.ids) {
            this.modelPpg(ppgModelRequest);
        } else {
            this.openSnackBarAlert('No se seleccionaron datos, vaya a la tabla para seleccionarlos', 'Ir a tabla').onAction().subscribe(() => {
                this.router.navigate(['/']);
            });
        }
    }

    ngOnInit(): void {
    }

    drawMatrix() {
        const axis = [1, 2, 3]
        const axes = {
            z: this.model!.confussion_matrix.map((row) => row.map((value) => value / this.model!.test_set_size * 100)),
            x: axis,
            y: axis
        };

        const values = this.model!.confussion_matrix.flat();
        const min = values.reduce((a, b) => Math.min(a, b));
        const max = values.reduce((a, b) => Math.max(a, b));

        const annotations: any[] = [];
        for (let i = 0; i < axes.y.length; i++) {
            for (let j = 0; j < axes.x.length; j++) {
                const currentValue = this.model!.confussion_matrix[i][j];
                const result = {
                    xref: 'x1',
                    yref: 'y1',
                    x: axes.x[j],
                    y: axes.y[i],
                    text: this.model!.confussion_matrix[i][j],
                    showarrow: false,
                    font: {
                        color: currentValue < (max - min) / 2 ? 'black' : 'white'
                    }
                };

                annotations.push(result);
            }
        }

        Plotly.newPlot(this.matrix?.nativeElement, [
            {
                ...axes,
                type: 'heatmap',
                colorscale: 'Blues',
                reversescale: true,
            }
        ], {
            title: 'Matriz de confusión',
            annotations,
            xaxis: {
                title: {
                    text: 'Clase predecida'
                },
                dtick: 1,
                side: 'bottom'
            },
            yaxis: {
                title: {
                    text: 'Clase real'
                },
                dtick: 1
            }
        });
    }

    modelPpg(ppgModelRequest: PpgModelRequest) {
        this.ppgService.model(ppgModelRequest).subscribe((ppgModelResponse: PpgModelResponse) => {
            this.loading = false;
            this.model = ppgModelResponse;
            console.log(this.model);
            this.drawMatrix();
        }, (error) => {
            this.openSnackBarAlert('Error al cargar contenido', 'Regresar').onAction().subscribe(() => {
                this.router.navigate(['/']);
            });
        });
    }

    downloadCsv() {
        const data: Blob = new Blob([this.model!.csv], {
            type: "text/csv;charset=utf-8"
        });

        saveAs(data, "set-entrenamiento.csv");
    };

    openSnackBarAlert(message: string, action: string) {
        return this._snackBar.open(message, action, {
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
            duration: 5000,
        });
    }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PpgComponent } from './ppg.component';

describe('PpgComponent', () => {
  let component: PpgComponent;
  let fixture: ComponentFixture<PpgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PpgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PpgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

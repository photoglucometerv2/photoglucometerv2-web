import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PpgShowResponse } from '../models/ppg';
import { PpgService } from '../services/ppg.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { ScaleType } from '@swimlane/ngx-charts';
import * as shape from 'd3-shape';
import { MatDialog } from '@angular/material/dialog';
import { DestroyPpgComponent } from '../destroy-ppg/destroy-ppg.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';

declare const Plotly: any;

@Component({
    selector: 'app-ppg',
    templateUrl: './ppg.component.html',
    styleUrls: ['./ppg.component.css']
})
export class PpgComponent implements OnInit {
    loading = true;
    id?: number;
    ppg?: PpgShowResponse;
    testing = "[[545, 365, 580], [563, 453, 575], [558, 496, 566], [563, 516, 578], [580, 545, 582]]";
    displayedColumns: string[] = [
        "id",
        "bp_systolic",
        "bp_diastolic",
        "true_gluco",
        "subject_age",
        "subject_height",
        "subject_weight",
        "subject_sex",
        "subject_isdiabetic",
        "subject_email",
        "dim_corr",
        "hurst_exp",
        "lyapunov_exp",
        "delete",
    ];
    dataSource = new MatTableDataSource<PpgShowResponse>([]);
    ppgSeries: number[] = [];
    ppgAltSeries: number[] = [];
    ecgSeries: number[] = [];
    ppgAttractor: any = {};
    ppgAltAttractor: any = {};
    ecgAttractor: any = {};

    constructor(
        private ppgService: PpgService,
        private route: ActivatedRoute,
        public dialog: MatDialog,
        private router: Router,
        private _snackBar: MatSnackBar
    ) { }

    ngOnInit(): void {
        this.id = Number(this.route.snapshot.paramMap.get('id'));
        this.showPpg();
    }

    showPpg() {
        this.ppgService.show(this.id!).subscribe((ppgShowResponse) => {
            this.ppg = ppgShowResponse;
            this.loading = false;

            this.ppgSeries = this.formatSeries(ppgShowResponse.ppg);
            this.ppgAltSeries = this.formatSeries(ppgShowResponse.ppg_alt);
            this.ecgSeries = this.formatSeries(ppgShowResponse.ecg);

            this.ppgAttractor = JSON.parse(ppgShowResponse.attractor_ppg);
            this.ppgAltAttractor = JSON.parse(ppgShowResponse.attractor_alt);
            this.ecgAttractor = JSON.parse(ppgShowResponse.attractor_ecg);

            this.dataSource.data = [this.ppg];
        }, (error) => {
            this.openSnackBarAlert('Error al cargar contenido', 'Regresar').onAction().subscribe(() => {
                this.router.navigate(['/']);
            });
        });
    }

    formatSeries(ppg: string) {
        return ppg.split(",").map((value) => Number(value));
    }

    openDestroyPpgDialog() {
        const dialogRef = this.dialog.open(DestroyPpgComponent, {
            width: '500px',
            data: {
                id: this.ppg?.id
            }
        });

        dialogRef.afterClosed().subscribe((wasDeleted) => {
            if (wasDeleted) {
                this.router.navigate(['/']);
            }
        });
    }

    openSnackBarAlert(message: string, action: string) {
        return this._snackBar.open(message, action, {
            horizontalPosition: 'center',
            verticalPosition: 'bottom',
            duration: 5000,
        });
    }
}

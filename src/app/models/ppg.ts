export class PpgIndexResponseItem {
    id!: number;
    bp_systolic!: number;
    bp_diastolic!: number;
    true_gluco!: number;
    subject_age!: number;
    subject_height!: number;
    subject_weight!: number;
    subject_sex!: string;
    subject_isdiabetic!: boolean;
    subject_email!: string;
    dim_corr_ppg!: number;
    hurst_exp_ppg!: number;
    lyapunov_exp_ppg!: number;
    processing_status_ppg!: number;
    dim_corr_alt!: number;
    hurst_exp_alt!: number;
    lyapunov_exp_alt!: number;
    processing_status_alt!: number;
    dim_corr_ecg!: number;
    hurst_exp_ecg!: number;
    lyapunov_exp_ecg!: number;
    processing_status_ecg!: number;
}

export class PpgIndexResponse {
    ppgs!: PpgIndexResponseItem[];
    total!: number
}

export class PpgShowResponse {
    id!: number;
    ppg!: string;
    ppg_alt!: string;
    ecg!: string;
    bp_systolic!: number;
    bp_diastolic!: number;
    true_gluco!: number;
    subject_age!: number;
    subject_height!: number;
    subject_weight!: number;
    subject_sex!: string;
    subject_isdiabetic!: boolean;
    subject_email!: string;
    dim_corr_ppg!: number;
    hurst_exp_ppg!: number;
    lyapunov_exp_ppg!: number;
    attractor_ppg!: string;
    processing_status_ppg!: number;
    dim_corr_alt!: number;
    hurst_exp_alt!: number;
    lyapunov_exp_alt!: number;
    attractor_alt!: string;
    processing_status_alt!: number;
    dim_corr_ecg!: number;
    hurst_exp_ecg!: number;
    lyapunov_exp_ecg!: number;
    attractor_ecg!: string;
    processing_status_ecg!: number;
}

export class PpgModelRequest {
    ids!: number[];
}

export class PpgModelResponse {
    confussion_matrix!: number[][];
    accuracy_score!: number;
    test_set_size!: number;
    csv!: string;
}